var app = require('express')()
var http = require('http').createServer(app)
var io = require('socket.io')(http)

var users = {}
var userCount = 0

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

const serializeUser = (user) => ({
  id: user.id,
  x: user.x,
  y: user.y,
  color: user.color,
})

const serializeUsersHash = (users) => {
  return Object.keys(users).reduce((userHash, userId) => {
    userHash[userId] = serializeUser(users[userId])
    return userHash
  }, {})
}

const broadcastUserChange = (user, event) => {
  user.socket.broadcast.emit(event, serializeUser(user))
}

const userLeft = (user) => broadcastUserChange(user, 'user left')
const userJoined = (user) => broadcastUserChange(user, 'user joined')
const userMoved = (user) => broadcastUserChange(user, 'user moved')


var randomInt = (min, max) => Math.round((max-min) * Math.random()) + min
var randomColor = () => `rgb(${randomInt(0,255)},${randomInt(0,255)},${randomInt(0,255)})`

io.on('connection', (socket => {
  // TODO: Accept user ID from client.
  const newUser = {
    id: userCount,
    x: 0,
    y: 0,
    color: randomColor(),
    socket,
  }

  users[userCount] = newUser
  userCount++

  userJoined(newUser)

  socket.emit('user login', {
    me: serializeUser(newUser),
    users: serializeUsersHash(users),
  })

  socket.on('error', (error) => {
    console.log('error: ', error)
  })

  socket.on('disconnecting', () => {
    console.log(`Bye User ${newUser.id}!`)
    userLeft(newUser)
    delete users[newUser.id]
  })

  socket.on('user moved', (message) => {
    newUser.x = message.x
    newUser.y = message.y
    userMoved(newUser)
  })

  console.log(`Current users that are here are ${Object.keys(users)}`)
}))

http.listen(3000, () => {
  console.log('Listening on *:3000')
})
