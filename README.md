# Live Cursors

An experiment to see how easy it would be to use WebSockets to display cursors for multiple users.

## Instructions

1. Start the server
  - **With Docker:** `docker-compose up`
  - **Without Docker:** `yarn start` or `npm start`
2. Point your browser to `http://localhost:3000`
3. Do steps 1 & 2 again from more devices to see multiple cursors.  Wow.  Amazing. 😴
